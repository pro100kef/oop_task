public class SecondClass extends AbstractClass implements Interface {

    static char statProperty;

    // Initialization.
    {
        statProperty = '5';
        firstProperty = 500;
        secondProperty = "Five hundred";
    }

    SecondClass() {
        this.firstProperty = 1;
        this.secondProperty = "not Zero";
    }

    SecondClass(int firstParameter, String secondParameter) {
        this.firstProperty = firstParameter;
        this.secondProperty = secondParameter;
    }

    protected int getFirstProperty() { return this.firstProperty; }
    protected String getSecondProperty() { return this.secondProperty; }
    protected void setFirstProperty(int parameter) { this.firstProperty = parameter; }
    protected void setSecondProperty(String parameter) { this.secondProperty = parameter; }

    protected String UniqueMethod(String uniqueText) {
        return this.secondProperty += " " + uniqueText;
    }

    @Override
    protected void AbstractMethod() {
        /* secondProperty
        firstProperty */
        System.out.println("secondProperty = " + this.secondProperty);
        System.out.println("firstProperty = " + this.firstProperty);
    }

    @Override
    protected void ParentMethod() {
        System.out.println("It was a parent method");
        System.out.println("Now it's overridden SecondClass method");
    }

    @Override
    public int InterMethod1() {
        return 2;
    }

    @Override
    public String InterMethod2() {
        return "SecondClass";
    }
}