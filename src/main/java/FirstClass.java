import java.util.InputMismatchException;
import java.util.Scanner;

public class FirstClass extends AbstractClass implements Interface {

    FirstClass() {
        firstProperty = 322;
        secondProperty = "Solo";
    }

    FirstClass(int firstParameter, String secondParameter) {
        firstProperty = firstParameter;
        secondProperty = secondParameter;
    }

    protected int getFirstProperty() { return firstProperty; }
    protected String getSecondProperty() { return secondProperty; }
    protected void setFirstProperty(int parameter) { firstProperty = parameter; }
    protected void setSecondProperty(String parameter) { secondProperty = parameter; }

    protected int UniqueMethod(int uniqueNumber) {
        return firstProperty += uniqueNumber;
    }

    public void PublicMethod() {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.print("Enter the value to be added: ");
            UniqueMethod(scanner.nextInt());
        }
        catch(InputMismatchException ex){
            System.out.println("Error: enter correct type value");
        }
        System.out.printf("New value of firstProperty = %d", getFirstProperty());
    }

    @Override
    protected void AbstractMethod() {
        /* firstProperty
        secondProperty */
        System.out.println("firstProperty = " + firstProperty);
        System.out.println("secondProperty = " + secondProperty);
    }

    @Override
    protected void ParentMethod() {
        System.out.println("It was a parent method");
        System.out.println("Now it's overridden FirstClass method");
    }

    @Override
    public int InterMethod1() {
        return 1;
    }

    @Override
    public String InterMethod2() {
        return "FirstClass";
    }
}