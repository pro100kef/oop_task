public abstract class AbstractClass {
    protected int firstProperty;
    protected String secondProperty;

    protected abstract void AbstractMethod();
    protected void ParentMethod() {
        System.out.println("It's a parent method");
    }
}