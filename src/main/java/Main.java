public class Main {
    public static void main (String[] args) {
        // 'AbstractClass' is abstract. Cannot be instantiated.
        // AbstractClass abstractClass = new AbstractClass();

        // Initiating w\ empty constructors and setters (FirstClass & SecondClass).
        FirstClass childFirst = new FirstClass();
        childFirst.setFirstProperty(1000);
        SecondClass childSecond = new SecondClass();
        childSecond.setSecondProperty("Vlad");

        // Initiating w\ constructors (FirstClass & SecondClass).
        FirstClass firstClass = new FirstClass(99, "Bread");
        System.out.print(firstClass.getSecondProperty() + " ");
        SecondClass secondClass = new SecondClass(666, "Devil");
        System.out.println(secondClass.getSecondProperty());
        System.out.println();

        // Calling methods of firstClass object.
        /* If the superclass method is static, then the child
        methods need to be static too. The childFirst object is
        of the FirstClass type -> child method will execute. */
        /* If the superclass method is static final, then the child
        methods won't be able to overwrite the parent method. */
        childFirst.ParentMethod(); // Executes overridden method.

        childFirst.AbstractMethod();
        System.out.println(childFirst.UniqueMethod(111));
        System.out.println();

        // Calling methods of secondClass object.
        childSecond.ParentMethod();
        childSecond.AbstractMethod();
        System.out.println(childSecond.UniqueMethod("is Impressive"));
        System.out.println();

        /* Polymorphism. Type: AbstractClass.
        Reference: FirstClass. */
        AbstractClass parentFirst = new FirstClass();

        /* The superclass method was overridden
        by the child method */
        /* If the superclass method is static, then the child
        methods need to be static too. The parentFirst object is
        of the AbstractClass type -> superclass method. */
        /* If the superclass method is static final, then the child
        methods won't be able to overwrite the parent method. */
        parentFirst.ParentMethod();

        parentFirst.AbstractMethod();
        System.out.println();

        // UniqueMethod(int uniqueNumber) is not defined in the AbstractClass.
        //System.out.println(parentFirst.UniqueMethod(111));

        /* Polymorphism. Type: AbstractClass.
        Reference: SecondClass. */
        AbstractClass parentSecond = new SecondClass();

        parentSecond.ParentMethod();
        parentSecond.AbstractMethod();
        System.out.println();

        /* Array of objects initialization of the AbstractClass(parent) type.
        Objects childFirst and childSecond are up-casted to parent class automatically. */
        AbstractClass[] parentArray = { childFirst, childSecond, parentFirst, parentSecond };
        for (AbstractClass obj : parentArray) {
            obj.AbstractMethod();
            obj.ParentMethod();
            System.out.println();
            /* In this example, we can only call the
            methods that are defined in the parent class.
            Overridden methods are executed. */
        }

        // Initialization of the ThirdClass. It doesn't have a parent class.
        ThirdClass classThird = new ThirdClass();

        // Array of objects initialization of the Interface type.
        /* The objects childFirst, childSecond and classThird are implementing the Interface.
        The objects parentFirst and parentSecond are of a superclass type so we need to
        cast them to the Interface type. These objects are referencing child
        classes that are implementing Interface, so the cast will do just fine. */
        Interface[] interArray = { childFirst, childSecond, (Interface) parentFirst,
                (Interface) parentSecond, classThird };

        for (Interface obj : interArray) {
            System.out.println(obj.InterMethod1());
            System.out.println(obj.InterMethod2());
            System.out.println();
            /* In this example, we can only call the
            methods that are defined in the Interface class.
            Overridden methods are executed. */
        }
    }
}