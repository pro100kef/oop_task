public class ThirdClass implements Interface {
    protected float firstProperty;
    protected String secondProperty;

    protected float firstMethod(float parameter) {
         return firstProperty = firstProperty * parameter;
    }

    protected String secondMethod(String parameter) {
        return secondProperty = parameter + " " + secondProperty;
    }

    @Override
    public int InterMethod1() {
        return 3;
    }

    @Override
    public String InterMethod2() {
        return "ThirdClass";
    }
}